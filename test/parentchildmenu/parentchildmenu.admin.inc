<?php
// $Id$

/**
 * @file
 * Admin page callbacks for the parentchildmenu module.
 */
  
function parentchildmenu_admin_form($item = NULL) {

  $node_types = node_get_types();
  foreach ($node_types as $node_type => $node_type_info) {
    $nodetype_options[$node_type] = $node_type_info->name;
  }
  
  $form['parentchildmenu_nodetypes'] = array(
     '#type' => 'checkboxes',
     '#title' => t('Content types which will be able to display a parent-child menu block, and for which a parent node can be selected'),
     '#options' => $nodetype_options,
     '#description' => (t('The node types for which you would like to be able to select a parent node. By default, the field that will be used to enter the name of the node on a node form will be an autocomplete type - you can change this by editing the node type. NOTE: after you have added a node type to this list, if you then want to remove the node reference field from that type, you will need to uncheck it here, AND remove the field from the node type (using the node type edit form).')),
     '#default_value' => variable_get('parentchildmenu_nodetypes', array()),
  );

  $form['parentchildmenu_options'] = array(
     '#type' => 'checkboxes',
     '#title' => t('What to include in the parent-child menu'),
     '#options' => array(
                    'parent' => t('Parent node of the node being displayed'),
                    'siblings' => t('Sibling nodes - nodes whose parent is the same as that of the node being displayed'),
                    'children' => t('Child nodes - nodes whose parent is the node being displayed'),
                    'site-map-link' => t('A link to the Parent Child Menu site map page at <a href="@site-map">parentchildmenu/sitemap</a>.', array('@site-map' => url('parentchildmenu/sitemap'))),
                    ),
     '#default_value' => variable_get('parentchildmenu_options', array()),
  );

  $form['parentchildmenu_sitemap_title'] = array(
     '#type' => 'textfield',
     '#title' => t('Title of the Parent Child Menu site map page'),
     '#default_value' => variable_get('parentchildmenu_sitemap_title', t('Site map')),
  );
  
  $form['#submit'][] = 'parentchildmenu_admin_form_submit';
  // Note: system_settings_form does the work of setting variables!
  return system_settings_form($form); 
}

/**
* Update nodereference fields as appropriate.
* 
* @param mixed $form
* @param mixed $form_state
*/
function parentchildmenu_admin_form_submit($form, $form_state) {
  // Hide the field (automatically don't display - it's a default option we can set...
  module_load_include('inc', 'content', 'includes/content.crud');
  
  foreach ($form_state['values']['parentchildmenu_nodetypes'] as $type => $value) {
    $node_reference_field = content_fields('field_pcm_node_parent', $type);
    if ($value && node_get_types('name', $type)) {
      if (empty($node_reference_field)) {
        content_field_instance_create(parentchildmenu_default_node_referencefield($type, $form_state));
        $node_types_with_reference_field_added[] = node_get_types('name', $type);
      }
      else {
        $unchanged_node_types[] = node_get_types('name', $type);
      }
    }
    // If the node type is not selected, but the node_reference field is not empty, record this for a message (and it hadn't already been deselected)
    elseif (!empty($node_reference_field) && $form['parentchildmenu_nodetypes']['#default_value'][$type] !== $form_state['values']['parentchildmenu_nodetypes'][$type]) {
      $types_with_fields_selected_for_deletion[] = node_get_types('name', $type);
    }
  }
  
  if (!empty($node_types_with_reference_field_added)) {
    $message = t('A new node reference field has been added to each of the following content types:');
    $message .= '<ul>';
    foreach ($node_types_with_reference_field_added as $added_to_node_type_name) {
      $message .= '<li>' . $added_to_node_type_name . '</li>';
    }
    $message .= '</ul>';
    drupal_set_message($message, 'status');
  }
  
  if (!empty($unchanged_node_types) && $form['parentchildmenu_nodetypes']['#default_value'] !== $form_state['values']['parentchildmenu_nodetypes']) {
    $message = t('The node reference field for each of the following node types has NOT been changed. You may want to review these fields by going to the appropriate node type edit pages:');
    $message .= '<ul>';
    foreach ($unchanged_node_types as $unchanged_node_type_name) {
      $message .= '<li>' . $unchanged_node_type_name . '</li>';
    }
    $message .= '</ul>';
    drupal_set_message($message, 'status');
  }
  
  if (!empty($types_with_fields_selected_for_deletion)) {
    $message = t('The following node types will no longer have a block with a parent-child menu available to them. The node reference fields for each type (which contains data about their parents) has NOT been deleted. If you want to delete these fields you should go to the node type edit page and delete the node parent field.');
    $message .= '<ul>';
    foreach ($types_with_fields_selected_for_deletion as $node_type_name) {
      $message .= '<li>' . $node_type_name . '</li>';
    }
    $message .= '</ul>';
    drupal_set_message($message, 'status');
  }
}


/**
* Returns the default node reference field - (to be added to relevant node types
* which do not already have this field for example).
* The $form_state is used for the node types that it will contain:
* ($form_state['parentchildmenu_nodetypes']) - i.e. the default node types for a new node
* reference to refer to are all the types which have been selected for being a node reference.
* 
* @param mixed $form_state
* @return mixed
*/
function parentchildmenu_default_node_referencefield($node_type, $form_state) {
  return array ( 
    'field_name' => 'field_pcm_node_parent', 
    'type_name' => $node_type, 
    'display_settings' => array ( 
      'label' => array ( 
        'format' => 'above', 
        'exclude' => 1, ), 
      'teaser' => array ( 
        'format' => 'default', 
        'exclude' => 1, ), 
      'full' => array ( 
        'format' => 'default', 
        'exclude' => 1, ), 
      4 => array ( 
        'format' => 'default', 
        'exclude' => 0, ), 
      2 => array ( 
        'format' => 'default', 
        'exclude' => 0, ), 
      3 => array ( 
        'format' => 'default', 
        'exclude' => 0, ), 
      'email_plain' => array ( 
        'format' => 'default', 
        'exclude' => 0, ), 
      'email_html' => array ( 
        'format' => 'default', 
        'exclude' => 0, ), 
      'token' => array ( 
        'format' => 'default', 
        'exclude' => 0, ), 
      ), 
      'widget_active' => '1', 
      'type' => 'nodereference', 
      'required' => '0', 
      'multiple' => '0', 
      'db_storage' => '1', 
      'module' => 'nodereference', 
      'active' => '1', 
      'locked' => '0', 
      'columns' => array ( 
        'nid' => array ( 
          'type' => 'int', 
          'unsigned' => true, 
          'not null' => false, 
          'index' => true, 
          ), 
        ), 
      'referenceable_types' => $form_state['values']['parentchildmenu_nodetypes'], 
      'widget' => array ( 
        'autocomplete_match' => 'starts_with', 
        'size' => '60', 
        'default_value' => array ( 
          0 => array ( 
            'nid' => NULL, 
            '_error_element' => 'default_value_widget][field_pcm_node_parent][0][nid][nid', 
            ), 
          ), 
        'default_value_php' => NULL, 
        'label' => 'Parent Page', 
        'weight' => '31', 
        'description' => 'Enter the title of the node which you would like to be the parent of this one. This field was added by the parentchildmenu module.', 
        'type' => 'nodereference_autocomplete', 
        'module' => 'nodereference', 
        ), 
      );
}