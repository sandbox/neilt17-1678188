// $Id$

Parent Child Menu
=================

Description
===========
The Parent Child Menu module is a simple way to create hierarchical menus on a site. Go to the settings page and include one or more content types. These content types will then have an extra field in which you can enter the name of a parent page.

A block is provided which can display a page's parent, siblings, and/or children. Go to the blocks configuration page to enable the block. The block will appear on any page which is either a parent or a child of another page or is both.

This module also automatically creates a site map from these pages at parentchildmenu/sitemap.

Parent Child Menu Block
=======================
The block can be configured to show links to any combination of the following:
* The parent node of the current node;
* The siblings of the current node (i.e. nodes with the same parent);
* The child nodes of the current node (all nodes which have the current node as their parent).

Styling
=======
CSS classes allow you to theme the links to the parent node, sibling nodes, and child nodes separately - child nodes are shown as an unordered list. The site map can be similarly styled.


Trouble shooting
================
Avoid creating "loops" where for example a page is a parent of itself, or a page's parent is also that page's child - this could result in blocks which look a bit odd - with duplicated links, and page's may be missing from the site map.


Credits
=======
Developed by Neil Thompson (Commtap CIC).
